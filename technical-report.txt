Motivation

In the 21st century, clean water supply remains a worldwide problem, but many of us take clean water for granted. Developing countries suffer from water shortages, marine life suffers from the deadly and poisonous water dumped by heavy industries, and eventually, the one who suffers is ourselves. There has been ongoing education about worldwide water pollution issues, but we think the attention it gets is not enough. To raise awareness of water issues more comprehensively, we build this site to educate the public about water pollution and how we could help in small but significant efforts.


User Stories

API: GET charities/search
As a user, I want to be able to query your API for charities by name, goal, or other appropriate identifiers. The response should contain matching charity records and each record should contain important information about the charity.

API: GET charity location
When I use your API, I want to be able to find a location of a specific charity. This will make it easier to associate a region to a charity.

API: GET Issues by location
As a user of your API, I would like to be able to query your API for issues by location. The response should contain matching issues records and each record should contain important information about the location.


RESTful API

GET body_of_waters:
Returns the locations of different body of waters

Example:
{
}


Models

Water Issues:
-Cause
-Effect
-Severity

Body of waters (location):
-Type body of water
-Country (if available)
-Issues in area

Charities:
-Name
-Goal
-Link to donate
-Volunteer opportunities


Tools


hosting