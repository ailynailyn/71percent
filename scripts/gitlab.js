// Taken from Let Me Help summer project.

var fieldIds = ["commits"];
// var fieldIds = [2767841, 2765415, 2841978, 2847892, 2756347];
var members = ["ailyn", "jason", "yassin", "thomas", "brandon"];
members.sort();

console.log(fieldIds);
for (var member of members) {
    for (fieldId of fieldIds) {
        console.log(member + "-" + fieldId);
        document.getElementById(member + "-" + fieldId).innerHTML = "0" + " " + fieldId;
    }
}

// Getting first name lowercased, so get all letters before first non-alpha in string
// then return the name lowercased. Probably a better way to do this.
function getFormattedName(rawName) {
    var name = "";
    var i = 0;
    const letters = /^[a-zA-Z]+$/;
    while(rawName.charAt(i).match(letters) && i < rawName.length) {
        name += rawName.charAt(i);
        i++;
    }
    return name.toLowerCase(); 
}

function populateCommitsAndEmail(contributors) {
    var totalCommits = 0;
    var i = 0;
    for (var contributor of contributors) {
        var name = members[i];
        console.log(name + "-" + fieldId);
        console.log(name + " " + contributor["commits"] + " commits");
        document.getElementById(name + "-" + "commits").innerHTML = contributor["commits"] + " " + "commits";
        totalCommits += contributor["commits"];
        i += 1;
    }
    document.getElementById("project-commits").innerHTML = totalCommits + " " + "commits"
}

function populateAvatarsAndIssues(issues) {
    var member_issues = {}
    for (var member of members) {
        member_issues[member] = {}
    }

    var totalIssues = 0;
    for (var issue of issues) {
        for (var assignee of issue["assignees"]) {
            var name = getFormattedName(assignee["name"])
            if (!("issues" in member_issues[name])) {
                member_issues[name]["issues"] = 0
            }
            member_issues[name]["issues"] += 1
            member_issues[name]["image"] = assignee["avatar_url"]
        }
        totalIssues += 1
    }
    for (var member in member_issues) {
        var avatar = member_issues[member]["image"]
        var assigned_issues = member_issues[member]["issues"]
        document.getElementById(member + "-" + "image").src = avatar;
        document.getElementById(member + "-" + "issues").innerHTML = "" + assigned_issues + " issues";
    }
    document.getElementById("project-issues").innerHTML = totalIssues + " " + "issues"
}

var ajax = new XMLHttpRequest();
ajax.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        const contributors = JSON.parse(this.responseText).sort();
        console.log(contributors);
        populateCommitsAndEmail(contributors);
    }
};
ajax.open("GET", "https://gitlab.com/api/v4/projects/8634714/repository/contributors");
ajax.send();

var ajax2 = new XMLHttpRequest();
ajax2.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        const issues = JSON.parse(this.responseText);
        console.log(issues);
        populateAvatarsAndIssues(issues);
    }
};
ajax2.open("GET", "https://gitlab.com/api/v4/projects/8634714/issues");
ajax2.send();
